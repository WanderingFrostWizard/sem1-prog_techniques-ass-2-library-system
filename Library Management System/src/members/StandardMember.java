package members;

public class StandardMember extends Member
{
	private final static int MAXIMUMCREDIT = 30;	// EACH STANDARD MEMBER HAS A FIXED $30 MAXIMUM BORROWING CREDIT
	
    public StandardMember(String standardMemberID, String standardMemberName)
    {
    	super(standardMemberID, standardMemberName, MAXIMUMCREDIT);
    }
	
    
	//Standard members are not allowed to return a (late) book if their current balance will become negative after paying the
	// late penalty fee. In such cases, the current credit of a member should first be restored to the initial maximum value.	  
    @Override
	public boolean checkAllowedCreditOverdraw(int lateFee) 
	{
    	if ( lateFee >= super.getCurrentCredit() )
    	{
    		return true;
    	}
    	
    	System.out.println("Standard Member currently doesn't have enough credit to pay the late fee on this item");
		return false;
	}
	
}  // END OF CLASS