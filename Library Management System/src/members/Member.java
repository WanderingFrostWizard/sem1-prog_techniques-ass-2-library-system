package members;

import java.util.Scanner;

import holdings.Holding;
import mainMenu.DateTime;

public abstract class Member 
{
	//public final Scanner keyboardMember = new Scanner(System.in);  // SEPARATE TO THE ONE IN MainMenu CLASS

	
    private String memberID;	// ID NUMBER ASSIGNED TO THE MEMBER
    private String fullName;	// FULL NAME OF THE MEMBER
    private int maxCredit;		// 
    private int currentCredit;	// MEMBERS CURRENT BORROWING CREDIT
    
    private boolean isActive = true;	// IF MEMBER IS ACTIVE
    
    // LIST OF ALL ITEMS BORROWED
    Holding[] borrowing = new Holding[15];	
    
    

    // CONSTRUCTORS FOR THE MEMBER CLASS
	public Member(String memberID, String fullName, int credit) 
	{
		this.memberID = memberID;
		this.fullName = fullName;
		this.currentCredit = credit;
	}
	

    // METHODS FOR THE MEMBER CLASS

	public String getID()  // DONE
	{
		return this.memberID;
	}
	
	public String getFullName() // DONE
    {
		return fullName;
    }
		
	public boolean getStatus()	// DONE
	{
		return isActive;
	}
	
	public int getMaxCredit()	// DONE
	{
		return maxCredit;
	}
	
	public int getCurrentCredit()	// ADDED THIS GETTER TO ALLOW checkAllowedCreditOverdraw TO WORK IN SUBCLASSES
	{
		return currentCredit;
	}
	
	public int calculateRemainingCredit(int lateFee)  
	{
		currentCredit = currentCredit - lateFee;
		return 0;	// NO RETURN VALUE REQUIRED	
	}
	
	public void resetCredit()  	// DONE
	{
		System.out.println("This Members credit has been reset to $" + maxCredit);
		currentCredit = maxCredit;
	}
	
	public boolean activate()
	{
		return false;
	}
	
	public boolean deactivate()  
	{
		return false;
	}
	
	public Holding[] getCurrentHoldings() 	// DONE  NOT CURRENTLY USED
	{
		return borrowing;
	}
	
	// NOT REQUIRED
	//public boolean updateRemainingCredit(int loanFee) {return false; 	}
	
	// IMPLEMENTED WITHIN THE SUBCLASSES, TO ALLOW FOR DIFFERENCES IN PROCEDURE
	public abstract boolean checkAllowedCreditOverdraw(int lateFee);
	
	public boolean borrowHolding(Holding holding)	// DONE - HOPEFULLY
	{
		int requiredCredit = holding.getDefaultLoanFee();
		
		if ( isActive == false )
		{
			System.out.println("Member is NOT currently Active within the system");
		}
		else if ( currentCredit < requiredCredit )
		{
			System.out.println("Member requires $" + requiredCredit + " in Credit, but currently has $" + currentCredit );
		}
		else if ( ( isActive ) && ( currentCredit >= requiredCredit ) )
		{
			// FIND AN EMPTY SPOT IN THE MEMBERS borrowing ARRAY
			for (int i = 0; i < borrowing.length; i++)
			{
				if (borrowing[i] == null)
				{
					borrowing[i] = holding;	// ADD THE BORROWED HOLDING TO THE MEMBERS ARRAY 
					break;
				}			
			}
			currentCredit = currentCredit - requiredCredit;
			return true;	// ITEM HAS SUCCESSFULLY BEEN BORROWED
		}
		return false;
	}
	
	
	// THIS FUNCTION RETURNS THE HOLDING TO THE SYSTEM ( BY REMOVING ITS DETAILS FROM THE MEMBER )
	public boolean returnHolding(Holding holding, DateTime returnDate)
	{
		// dateReturned IS NOT REQUIRED IN MY VERSION
		
		
		// FIND THE HOLDING TO BE RETURNED WITHIN THE MEMBERS BORROWING ARRAY
		for ( int i = 0; i < borrowing.length; i++)
		{
			// SKIP null OBJECTS
			if ( ( borrowing[ i ] != null ) && ( borrowing[ i ].equals( holding ) ) )
			{
				borrowing[ i ] = null;
				System.out.println("Holding successfully removed from Members Borrowing List");
				return true;
			}
			else if ( i == borrowing.length - 1)
			{
				System.out.println("This member has NOT borrowed this holding");
			}
		}

		return false;
	}
	
	public void print()
	{
		System.out.println("==================================================");
		System.out.println("Member ID: " + memberID + " Full Name: " + fullName);
        System.out.println("Current Credit: $" + currentCredit);
        System.out.println("");
        System.out.println("Items currently on Loan");
        
		for (int i = 0; i < borrowing.length; i++)
		{
			if (borrowing[i] != null)
			{
				borrowing[i].print();
			}
		}
	}
	
	public String toString()
	{
		
		/* THIS FUNCTION IS TO BE USED TO CONVERT THE DATA FROM THE HOLDINGS CLASS INTO A STRING FORMAT
		AND OUTPUT THE VALUES TO A FILE
		
		I DEARLY WANTED TO TRY TO INPUT THIS FUNCTION, BUT RAN OUT OF TIME.
		 */
	
	
	//The member class and its sub-classes should override the toString() method to provide a pre-determined string representation of the member. The format for the member representation separates each attribute via the use of a colon ":"
	//member_id:full_name:remaining_credit
	//e.g. p00001:Joe Bloggs:25
		return "";
	}
}

