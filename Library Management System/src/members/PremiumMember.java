package members;

public class PremiumMember extends Member
{
	private final static int MAXIMUMCREDIT = 45;	// EACH PREMIUM MEMBER HAS A FIXED $45 MAXIMUM BORROWING CREDIT
	
    public PremiumMember(String premiumMemberID, String premiumMemberName)
    {
        super(premiumMemberID, premiumMemberName, MAXIMUMCREDIT); 
    }
    
	// Premium members can return a holding even if this will result in a negative current balance. However, they won�t be
	// able to borrow any new holdings until their current credit is restored to the initial maximum value.
    @Override
	public boolean checkAllowedCreditOverdraw(int lateFee) 
	{
    	return true;
	}
	
}  // END OF CLASS
