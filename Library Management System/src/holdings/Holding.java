package holdings;

import mainMenu.DateTime;
import mainMenu.MainMenu;

//public class Holding (String holdingID, String title)
public abstract class Holding
{
    // INSTANCE VARIABLES FOR THE HOLDING CLASS
    private String holdingID;	// ID NUMBER FOR THE ITEM
    private String title;		// TITLE OF THE ITEM
    
    private boolean isActive = true;	// USED TO SHOW IF THE ITEM IS ACTIVE WITHIN THE SYSTEM
    private boolean onLoan = false;	// USED TO TRACK IF THE ITEM IS ON LOAN
    private DateTime borrowDate = null; 
  
    // ### CONSTRUCTOR FOR HOLDING CLASS ###
	public Holding(String holdingID, String title) 
	{
		// SETTERS
		this.holdingID = holdingID;
		this.title = title;
	}  // END OF THE HOLDING CONSTRUCTOR

	
	// ### METHODS FOR THE HOLDING CLASS ###

    public String getID()		// DONE
    {
    	return holdingID;        
    }
    
    public String getTitle()	// DONE
    {
    	return title;
    }
    
    public boolean getStatus()	// DONE
    {
    	return isActive;
    }     
    
    public boolean isOnLoan()	// DONE
    {
    	return onLoan;
    }

    public DateTime getBorrowDate()  	// DONE
    {
    	return borrowDate;
    }   	
    
    public abstract int getDefaultLoanFee();	// DONE

    public abstract int getMaxLoanPeriod();		// DONE

    public abstract int calculateLateFee(DateTime dateReturned);	// DONE
	
    public boolean borrowHolding() 
	{
    	// THIS FUNCTION IS USED TO SET THE borrowDate, AND THE onLoan VARIABLES IT DOESN'T NEED TO RETURN ANYTHING
    	// EACH OF THE CONDITIONS ARE CHECKED WITHIN THE MAIN FUNCTION
    	
		// A holding can only be borrowed if:
		// # it is currently active in the system
		// # it is not already on loan
    	 // ### CHECKED THE ABOVE IN MAIN MENU, USING GETTER FUNCTIONS ###
    	
    	// ### THEN SET THIS WHEN THE ITEM IS OK TO BE BORROWED
    	// ### AND AFTER THE MEMBER HAS BEEN CHECKED (Member Credit, Member Active etc)
		// If an item is borrowed, then it must have it�s borrowDate set to the current date
    	
    	onLoan = true;
    	borrowDate = MainMenu.todaysDate;	// SET THE borrowDate TO TODAYS DATE
    	
    	return false;	// ELSE THIS FUNCTION RETURNS false
	}
	
	public boolean returnHolding(DateTime dateReturned)
	{
			// dateReturned IS NOT REQUIRED IN MY VERSION
    		onLoan = false;
    		borrowDate = null;
    		return true;	// NO DECISIONS NEED TO BE MADE, AS THEY HAVE ALREADY BEEN MADE IN THE MAIN MENU
	}
  
	public abstract void print();	// DONE

	public String toString() 		// TODO ############################################### 
	{
		/* THIS FUNCTION IS TO BE USED TO CONVERT THE DATA FROM THE HOLDINGS CLASS INTO A STRING FORMAT
			AND OUTPUT THE VALUES TO A FILE
			
			I DEARLY WANTED TO TRY TO INPUT THIS FUNCTION, BUT RAN OUT OF TIME.
		
		
		
	// The Holding classes should override the toString() method to provide a pre-determined string 
	//	representation of the holding. The format for the holding representation separates each attribute via
	//	the use of a colon
	// holding_code:holding_title:standard_loan_fee:max_loan_period
	// e.g. b000001:Intro to Java:10:28
	// v000001:Intro to Java 1:4:7
		
		*/
		return "";
	}
	
	public boolean activate() 		// UN-USED AT THIS STAGE
	{
		return false;
	}
	public boolean deactivate() 		// UN-USED AT THIS STAGE
	{
		return true;
	}


}  // END OF THE HOLDING CLASS

