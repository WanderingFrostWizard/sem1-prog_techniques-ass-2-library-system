package holdings;

import mainMenu.DateTime;

public class Video extends Holding
{
	private int loanFee = 0;
	private final int MAXLOANPERIOD = 7;	// VIDEOS HAVE A MAXIMUM LOAN PERIOD OF 7 DAYS
	
	// FOR RETURNING ITEMS
	private int lateFee = 0;
	private int daysBorrowed = 0;
	private int daysLate = 0;
	
	// CONSTRUCTORS
	public Video(String holdingID, String title, int loanFee) 
	{
		super(holdingID, title);	// GET AND SET DATA FROM HOLDING CLASS
	}
	
    @Override
    public int getDefaultLoanFee()	// DONE
    {
    	return loanFee;
    }
    
    @Override
    public int getMaxLoanPeriod()	// DONE
    {	
    	return MAXLOANPERIOD;
    }

    @Override
    public int calculateLateFee(DateTime dateReturned)
    {
		daysBorrowed =  DateTime.diffDays(dateReturned, super.getBorrowDate() );
		
		// TODO  USED TO HELP VALIDATE INPUT AT THIS STAGE
		System.out.println("borrowDate = " + super.getBorrowDate());
		System.out.println("dateReturned = " + dateReturned);		
		System.out.println("daysBorrowed = " + daysBorrowed);
    	
		if (daysBorrowed > getMaxLoanPeriod())
		{
			daysLate = daysBorrowed - getMaxLoanPeriod();
		    	lateFee = (int) (daysLate * loanFee * 0.5);
    	
		    // SHOW THE USER THE DAYS OVERDUE AND THE LATE FEE
	    	System.out.println("Days OVERDUE: " + daysLate);
	    	System.out.println("Late Fee is: $" + lateFee);
		}   	
		else// TODO    USED TO VALIDATE INPUT ###  if NOT OVERDUE, GENERALLY WOULD JUST CONTINUE  #####
		{
			System.out.println("ITEM WAS NOT OVERDUE");
		}

    	return lateFee;
    }
    
    @Override
	public void print()	// TODO MOVE TO HOLDINGS AND USE SINGLE FUNCTION
	{
    	System.out.println("");		
    	System.out.println("Item ID: " + getID() + " Title: " + getTitle() );
        System.out.println("Loan Fee: $" + loanFee + "\t Max Loan Period: " + MAXLOANPERIOD + " Days");
        System.out.println("Date Borrowed: " + getBorrowDate());
	}

}	// END OF public class Video extends Holding