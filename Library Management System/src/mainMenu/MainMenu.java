package mainMenu;

import java.util.Scanner;

import holdings.Book;
import holdings.Holding;
import holdings.Video;
import members.Member;
import members.PremiumMember;
import members.StandardMember;

public class MainMenu 
{
	// SYSTEM VARIABLES
	private Scanner keyboardMain = new Scanner(System.in);

	// GENERIC VARIABLES
	public final int MAXIMUMOBJECTS = 15;
	
	boolean validResponse = false;	// USED TO END FUNCTIONS AFTER INPUT HAS BEEN VALIDATED
	//boolean responseBool = false;	// FROM FUNCTIONS #################################
	private int menuEntry = 0;
	
	int responseInt = 0;
	String responseString = "";
	
	// SEARCHING VARIABLES
	String targetID = "";
	String currentID = "";
	int targetHoldingIndex = 99;
	int targetMemberIndex = 99;
	
	
	// DATE VARIABLES
	public static DateTime todaysDate = new DateTime();
	
	
	// HOLDING SPECIFIC VARIABLES
	private String holdingID = "";	// TEMPORARY USE ONLY
	private String title = "";		// TEMPORARY USE ONLY

	private Holding[] holdingList = new Holding[MAXIMUMOBJECTS];	// USED TO CONTAIN ALL THE HOLDINGS OBJECTS
	private int numberOfHoldings = 0; 	// TRACKS THE TOTAL NUMBER OF HOLDINGS 
	private char holdingType;	// USED TO CREATE THE holdingID's WHEN ADDING NEW HOLDINGS
	
	
	// MEMBER SPECIFIC VARIABLES
	private String memberID = "";	// TEMPORARY USE ONLY
	private String fullName = "";	// TEMPORARY USE ONLY
	
	private Member[] memberList = new Member[MAXIMUMOBJECTS];	// USED TO CONTAIN ALL THE MEMBER OBJECTS
	private int memberNo = 0;	// TRACKS THE TOTAL NUMBER OF MEMBERS
	private char memberType;
	
	public MainMenu()
	{
		CreateMenu();
	}
	
	
	private void CreateMenu()
	{
		// ADDS THE SAMPLE DATA FROM PART B
		addSampleData();	// TODO ############## ADD THE FILE SAVE, THEN SAVE TO A FILE, THEN REMOVE THIS ############
		
	    while (menuEntry != 99)
	    {
	    	System.out.println("");
	    	System.out.println(" 1 - Create New Holding (Book or Video)");
	    	System.out.println(" 2 - Remove Holding from System");
	    	System.out.println(" 3 - Create New Member");
	    	System.out.println(" 4 - Remove Member from System");
	    	System.out.println(" 5 - Borrow Holding");
	    	System.out.println(" 6 - Return Holding");
	    	System.out.println(" 7 - Print ALL Holdings");
	    	System.out.println(" 8 - Print ALL Members");
	    	System.out.println(" 9 - Print specific Holding");
	    	System.out.println("10 - Print specific Member");
	    	System.out.println("11 - ### SAVE TO FILE");
	    	System.out.println("12 - ### LOAD FROM FILE");
	        System.out.println("99 - Exit program");            
	        
	        menuEntry = keyboardMain.nextInt();
	        keyboardMain.nextLine();
	        
	        switch(menuEntry)
	        {
	            case 1:	// CREATE HOLDING - OPENS SEPARATE MENU
	            {
	            	createHoldingMenu();
	                break;
	            }
	            case 2:
	            {
	            	menuRemoveHolding();
		        	break;
	            }
	            case 3:	// CREATE MEMBER - OPENS SEPARATE MENU
	            {
	            	createMemberMenu();
		        	break;
	            }
	            case 4:
	            {
	            	menuRemoveMember();
	            	break;
	            }
	            case 5:
	            {
	            	menuBorrowHoldings();
	            	break;
	            }	
	            case 6:	
	            {
	            	menuReturnHoldings();
	            	break;
	            }
		        case 7:	// PRINT ALL HOLDINGS
		        {
		        	for (int i = 0; i < holdingList.length; i++)
		        	{
		        		if (holdingList[i] != null)
						{
							holdingList[i].print();
						}
		        	}
		        	break;
		        }
		        case 8:	// PRINT ALL MEMBERS
		        {	        		
		        	System.out.println("");
		        	for (int i = 0; i < memberList.length; i++)
		        	{
		        		if (memberList[i] != null)
						{
							memberList[i].print();
						}
		        	}
		        	break;
		        }
		        case 9:
		        {
		        	printSpecificHolding();
	                break;
	            }
		        case 10:
		        {
		        	printSpecificMember();
	                break;
	            }
		        case 11: // SAVE TO FILE
		        {
		        
		        	break;
		        }
		        case 12: // LOAD FROM FILE
		        {
		        	
		        	break;
		        }
		        
		        case 99: {} // EXIT - CHARACTER ENTERED ENDS WHILE LOOP
		       
	        }  // END OF switch(menuEntry)
	        
	        pressAnyKey();
	   } // END OF while (menuEntry != 9)    
	    
	}	// END OF THE MainMenu METHOD
	
	
	
	
	
	// ### 1 ### CREATING NEW HOLDINGS
	private void createHoldingMenu()
	{
		menuEntry = 0;
		
		while ((menuEntry < 1 || menuEntry > 3))
        {
    		System.out.println("");
    		System.out.println("Please select the type of Holding you wish to Create");
    		System.out.println("enter 1 to Create a Book");
    		System.out.println("enter 2 to Create a Video");
    		System.out.println("enter 3 to Return to Main Menu");
    	
    		menuEntry = keyboardMain.nextInt();
    		keyboardMain.nextLine();
    		
    		switch(menuEntry)
    		{
	            case 1:
	            {
	            	menuCreateBook();
	                break;
	            }
	            case 2:
	            {
	            	menuCreateVideo();
		            break;
	            }
	            case 3:		// ALLOWS THE USER TO RETURN TO THE MAIN MENU
	            {
	            	break;
	            }
    		} // END OF SWITCH(menuEntry)
    		
        }  // END OF while LOOP
    	
	}	// END OF private void menuCreateHolding()
	
	private void menuCreateBook()
	{
        if (numberOfHoldings < MAXIMUMOBJECTS)	
        {	            	
        	holdingType = 'b'; 
        	holdingID = createHoldingID(holdingType); // CREATE A NEW holdingID WITH THE BOOK PREFIX
        	title = getString("Title of the Item");  
        	
            // CREATE THE BOOK OBJECT AND ADD THE OBJECT TO THE HoldingList ARRAY
            holdingList[numberOfHoldings] = new Book(holdingID, title);
            System.out.println("Newly created holding details: ");
            holdingList[numberOfHoldings].print();
            numberOfHoldings++;
        }
        else	// holdingList ARRAY IS FULL
        {
        	maxObjectsReached();  // DISPLAY AN ERROR MESSAGE TO SHOW THIS
        }
	}
	
	private void menuCreateVideo()
	{
        if (numberOfHoldings < MAXIMUMOBJECTS)
        {           
        	holdingType = 'v';
        	holdingID = createHoldingID(holdingType); // CREATE A NEW holdingID WITH THE VIDEO PREFIX
        	title = getString("Title of the Item");
        	int loanFee = getLoanFee();
        	
        	// CREATE THE VIDEO OBJECT AND ADD THE OBJECT TO THE HoldingList ARRAY
            holdingList[numberOfHoldings] = new Video(holdingID, title, loanFee);  
            System.out.println("Newly created holding details: ");
            holdingList[numberOfHoldings].print();
            numberOfHoldings++;
        }
        else	// holdingList ARRAY IS FULL
        {
        	maxObjectsReached();  // DISPLAY AN ERROR MESSAGE TO SHOW THIS
        }
	}
	
	// USED TO AUTOMATICALLY CREATE THE holdingID's WHEN ADDING NEW OBJECTS
	private String createHoldingID(char holdingType) 
	{
		if (numberOfHoldings < 10)
		{
			holdingID = holdingType + "00000" + numberOfHoldings;
		}
		else
		{
			holdingID = holdingType + "0000" + numberOfHoldings; 
		}
		return holdingID;
	}
	
	// ASK USER TO ENTER THE CURRENT LOAN FEE WHEN CREATING VIDEO OBJECTS
	private int getLoanFee() 
	{
		responseInt = 0;
		
		while (responseInt != 4 && responseInt != 6)
		{
			System.out.println("Please enter the standard Loan Fee for this Video $4 or $6");
			responseInt = keyboardMain.nextInt();
		}
		return responseInt;
	}
	
	
	// ### 2 ### REMOVE HOLDING
	private void menuRemoveHolding()
	{
		System.out.println("Please enter the holdingID of the item to be REMOVED, or enter any other value to return to the Main Menu: ");
		targetID = keyboardMain.nextLine();
		
		for (int i = 0; i < holdingList.length; i++)
		{
			if ( (holdingList[i] != null) && (holdingList[i].getID().equals(targetID)) )
			{
				holdingList[i] = null;
				numberOfHoldings--;		// DECREASE THE TRACKING NUMBER OF HOLDING OBJECTS
				System.out.println("Holding was successfully REMOVED");
			}
			else if (i == holdingList.length)
			{
				System.out.println("Holding ID was NOT Found");
			}
		}
	}
	
	
	// ### 3 ### CREATING NEW MEMBERS
	private void createMemberMenu()
	{
		menuEntry = 0;
		
		while ((menuEntry < 1 || menuEntry > 3))
        {
    		System.out.println("");
    		System.out.println("Please select the type of Member you wish to Create");
    		System.out.println("enter 1 to Create a Standard Member");
    		System.out.println("enter 2 to Create a Premium Member");
    		System.out.println("enter 3 to Return to Main Menu");
    	
    		menuEntry = keyboardMain.nextInt();
    		keyboardMain.nextLine();
    		
    		switch(menuEntry)
    		{
	            case 1:
	            {
	            	menuCreateStandardMember();
	                break;
	            }
	            case 2:
	            {
	            	menuCreatePremiumMember();
		            break;
	            }
	            case 3:		// ALLOWS THE USER TO RETURN TO THE MAIN MENU
	            {
	            	break;
	            }
    		} // END OF SWITCH(menuEntry)
    		
        }  // END OF while LOOP
    	
	}	// END OF private void menuCreateHolding()
	
	private void menuCreateStandardMember()
	{
        if (memberList.length < MAXIMUMOBJECTS)	
        {	            	
        	memberType = 's'; 
        	memberID = createMemberID(memberType); // CREATE A NEW memberID WITH THE CORRECT PREFIX
        	fullName = getString("Full Name of the Member");
        	
        	// CREATE THE STANDARD MEMBER AND ADD THEM TO THE memberList ARRAY
            memberList[memberNo] = new StandardMember(memberID, fullName); 
            System.out.println("Newly created member details: ");
            memberList[memberNo].print(); 
            memberNo++;
        }
        else	// memberList ARRAY IS FULL
        {
        	maxObjectsReached();  // DISPLAY AN ERROR MESSAGE TO SHOW THIS
        }
	}
	
	private void menuCreatePremiumMember()
	{
        if (memberList.length < MAXIMUMOBJECTS)
        {	            	
        	memberType = 'p'; 
        	memberID = createMemberID(memberType); // CREATE A NEW memberID WITH THE CORRECT PREFIX
        	fullName = getString("Full Name of the Member");
        	
        	// CREATE THE PREMIUM MEMBER AND ADD THEM TO THE memberList ARRAY
        	memberList[memberNo] = new PremiumMember(memberID, fullName); 
        	System.out.println("Newly created member details: ");
            memberList[memberNo].print(); 
            memberNo++;
        }
        else	// memberList ARRAY IS FULL
        {
        	maxObjectsReached();  // DISPLAY AN ERROR MESSAGE TO SHOW THIS
        }
	}
	
	// USED TO AUTOMATICALLY CREATE THE memberID's WHEN ADDING NEW OBJECTS
	private String createMemberID(char memberType) 
	{
		if (memberNo < 10)
		{
			memberID = memberType + "00000" + memberNo;
		}
		else
		{
			memberID = memberType + "0000" + memberNo; 
		}
		return memberID;
	}

	
	// ### 4 ### REMOVE MEMBER
	private void menuRemoveMember()
	{
		System.out.println("Please enter the memberID of the member to be REMOVED, or enter any other value to return to the Main Menu: ");
		targetID = keyboardMain.nextLine();
		
		for (int i = 0; i < memberList.length; i++)
		{
			if ( (memberList[i] != null) && (memberList[i].getID().equals(targetID)) )
			{
				memberList[i] = null;
				memberNo--;		// DECREASE THE TRACKING NUMBER OF MEMBER OBJECTS
				System.out.println("Member was successfully REMOVED");
			}
			else if (i == memberList.length)
			{
				System.out.println("Member ID was NOT Found");
			}
		}
	}
	
	
	// ### 5 ### BORROW ITEMS
	private void menuBorrowHoldings()
	{
    	System.out.println("BORROW HOLDING");
    	
    	// ### CHECK HOLDING ###
    	
    	// ASK THE USER FOR THE HOLDING ID TO BE BORROWED ( VALIDATED ) AND STORE ITS INDEX WITHIN THE holdList ARRAYLIST
    	targetHoldingIndex = getHoldingID();
    	
    	// INVALID INPUT OR USER SELECTED TO QUIT FROM getHoldingID()
    	if ( targetHoldingIndex == 99 )
    	{
    		return;	
    	}
    	
    	boolean holdingActive = holdingList[targetHoldingIndex].getStatus();
    	boolean holdingOnLoan = holdingList[targetHoldingIndex].isOnLoan();
    	
    	// IF ITEM IS ACTIVE AND ITEM IS NOT ON LOAN, THEN THIS HOLDING CAN BE BORROWED, ELSE SHOW THE USER WHY IT CANT BE BORROWED
    	if (holdingActive == false)
    	{
			System.out.println("Item is currently NOT ACTIVE");
    	}
    	
    	if (holdingOnLoan == true)
		{
			System.out.println("Item is currently ON LOAN");
		}
    	else if ( ( holdingActive ) && ( holdingOnLoan == false ) )
    	{
    		// ### CHECK MEMBER ###
    		targetMemberIndex = getMemberID();
    		
        	// INVALID INPUT OR USER SELECTED TO QUIT FROM getHoldingID()
        	if ( targetMemberIndex == 99 )
        	{
        		return;	
        	}
    		
        	// IF THE ITEM CAN BE BORROWED
        	if ( memberList[targetMemberIndex].borrowHolding(holdingList[targetHoldingIndex]) )
        	{
        		// SET THE borrowDate AND onLoan VARIABLES WITHIN THE HOLDING CLASS
        		holdingList[targetHoldingIndex].borrowHolding();
        		System.out.println("ITEM HAS BEEN SUCCESSFULLY BORROWED");
        	}
    	}	// END OF if ( ( holdingActive ) && ( holdingOnLoan ) )
    	
	}	// END OF private void menuBorrowItems()

	
	// ### 6 ### RETURN ITEMS
	private void menuReturnHoldings()
	{
		// GET THE MEMBER AND HOLDING IDS
		targetMemberIndex = getMemberID();
		
		// INVALID INPUT OR USER SELECTS QUIT
		if ( targetMemberIndex == 99 )
			return;
					
		targetHoldingIndex = getHoldingID();
		
		// INVALID INPUT OR USER SELECTS QUIT
		if ( targetHoldingIndex == 99 )
			return;
		
		if ( memberHasBorrowedHolding( targetMemberIndex, targetHoldingIndex ) == false )
		{
			return;
		}
		
		boolean canReturnHolding = checkReturnHoldingRequirements( targetHoldingIndex );
		
		// CALCULATE LATE FEES ( IF ANY )
		int lateFee = holdingList[targetHoldingIndex].calculateLateFee(todaysDate);
							
		// CHECK TO SEE IF THE MEMBER HAS SUFFICIENT CREDIT TO PAY THE ABOVE FEES
		boolean hasCredit = memberList[targetMemberIndex].checkAllowedCreditOverdraw(lateFee);
		
		if ( canReturnHolding &&  hasCredit )
		{
			// REMOVE HOLDING FROM MEMBERS BORROWING ARRAY ( IF NOT FOUND, EXIT FUNCTION )
			if ( memberList[targetMemberIndex].returnHolding(holdingList[targetHoldingIndex], todaysDate) )
			{
				// DEDUCT LATE FEES
				memberList[targetMemberIndex].calculateRemainingCredit(lateFee);
				
				// SET onLoan AND SET borrowDate TO null
				holdingList[targetHoldingIndex].returnHolding(todaysDate);
			}
		}
	} 	// END OF private void menuReturnHoldings()
	
	private boolean memberHasBorrowedHolding( int targetMemberIndex, int targetHoldingIndex )
	{
		Holding[] membersBorrowed = memberList[targetMemberIndex].getCurrentHoldings();
		
		// FIND THE HOLDING TO BE RETURNED WITHIN THE MEMBERS BORROWING ARRAY
		for ( int i = 0; i < membersBorrowed.length; i++)
		{
			// SKIP null OBJECTS
			if ( ( membersBorrowed[ i ] != null ) && ( membersBorrowed[ i ].equals( holdingList[targetHoldingIndex] ) ) )
			{
				System.out.println("MEMBER HAS BORROWED THIS ITEM");
				return true;
			}
			else if ( i == membersBorrowed.length - 1)
			{
				System.out.println("This member has NOT borrowed this holding");
			}
		}
		return false;
	}
	
	
	private boolean checkReturnHoldingRequirements( int targetHoldingIndex )
	{
		// THIS FUNCTION CHECKS THE FOLLOWING
		// A holding can only be returned if:
		// # it is currently active in the system
		// # it is already on loan
		// # the return date is on or after the date on which the item was borrowed
		
		boolean isActive = holdingList[targetHoldingIndex].getStatus();
		boolean onLoan =  holdingList[targetHoldingIndex].isOnLoan();
		
    	if (isActive == false)
    	{
			System.out.println("Item is currently NOT ACTIVE");
			return false;
    	}
    	
    	if (onLoan == false)
		{
			System.out.println("Item is currently NOT ON LOAN");
			return false;
		}
    	
    	DateTime dateBorrowed = holdingList[targetHoldingIndex].getBorrowDate();
    	
    	// THE returnDate (TODAY) MUST BE ON OR AFTER THE dateBorrowed
		int daysLoaned = DateTime.diffDays( todaysDate, dateBorrowed );
		
    	// IF THE ITEM IS ACTIVE AND THE ITEM IS ON LOAN, THEN IT CAN BE RETURNED
    	if ( ( isActive ) && ( onLoan ) && ( daysLoaned <= 0 ) )
    	{
    		return true;
    	}
    	
    	return false;
	}	// END OF boolean checkReturnHoldingRequirements()
	
	
	// ### 9 ### PRINT SPECIFIC HOLDING
	private void printSpecificHolding()
	{
		System.out.println("Please enter the holdingID of the item to be printed: ");
		targetID = keyboardMain.nextLine();
		
		for (int i = 0; i < holdingList.length; i++)
		{
			if ( (holdingList[i] != null) && (holdingList[i].getID().equals(targetID)) )
			{
				holdingList[i].print();
			}
			else if (i == holdingList.length)
			{
				System.out.println("Holding ID was NOT Found");
			}
		}
	}
	
	
	// ### 10 ### PRINT SPECIFIC MEMBER
	private void printSpecificMember()
	{
    	System.out.println("Please enter the memberID of the member to be printed: ");
    	targetID = keyboardMain.nextLine();
    	
    	for (int i = 0; i < memberList.length; i++)
    	{
    		if ( (memberList[i] != null) && (memberList[i].getID().equals(targetID)) )
			{
				memberList[i].print();
			}
    		else if (i == memberList.length)
    		{
    			System.out.println("Member ID was NOT Found");
    		}
    	}
	}
	
	
	

	// ### GENERIC OR UTILITY FUNCTIONS ###
	
	// THIS IS A GENERIC FUNCTION THAT CAN BE USED FOR GETTING ANY STRING FROM THE USER, BY ACCEPTING A 
	// STRING OF CHARACTERS FROM CALLING FUNCTION TO DESCRIBE TO THE USER WHAT STRING THEY ARE REQUIRED TO INPUT
	private String getString(String stringRequired)		
	{
		validResponse = false;

	    // REPEAT UNTIL THE USER ENTERS A VALID RESPONSE
	    while (validResponse == false)
	    {
	        // ASK THE USER TO ENTER THE STRING REQUIRED (DESCRIBED BY stringRequired)
	        System.out.println("Please enter the " + stringRequired);      
	        responseString = keyboardMain.nextLine();

	        if (responseString.length() > 1)  // responseString MUST BE AT LEAST 1 CHARACTER LONG 
	        {	
	        	// FIRST CHARACTER CANNOT BE BLANK
	            if (Character.isLetter(responseString.charAt(0)))
	            {
	            	validResponse = true;
	            }
	            else
	            {
	                System.out.println("The first character MUST be a letter");
	            }
	        }
	        else
	        {
	        	// NOT ENOUGH CHARACTERS DETECTED
	        	System.out.println("Not Enough Characters detected");
	        } 
	    }  // END OF while (validResponse == false)
		return responseString;
	}	// END OF private String getString()
	

	// ASKS THE USER TO ENTER A VALID holdingID AND RETURNS ITS INDEX WITHIN THE holdingList ARRAYLIST
	private int getHoldingID()
	{
    	System.out.println("Please enter the holdingID of the Holding OR any other response to return to the Main Menu");
    	targetID = keyboardMain.nextLine();
	
		// THIS FUNCTION SCROLL THROUGH THE ENTIRE ARRAY, TO CHECK EACH ELEMENT
		for (int i = 0; i < holdingList.length; i++)
		{
    		// IF THE ID WAS NOT FOUND, DISPLAY THE RESULT FOR THE USER
    		if (i == holdingList.length - 1)
    		{
    			System.out.println("Holding " + targetID + " was not found");
    		}
    		
			// SKIP OVER NULL ENTRIES IN THE ARRAY
			if (holdingList[i] != null)
			{
				currentID = holdingList[i].getID();
	    		
				// CHECK THE holdingID OF THE TARGET HOLDING AGAINST THE targetID 
	    		if (currentID.equals(targetID))
	    		{
	    			return i;	// return the index of the member and exit the loop
	    		}
			}
			
		}  // END OF for (int i = 0; i < HoldingList.length; i++)
		return 99;
	}
		
	
	// ASKS THE USER TO ENTER A VALID memberID AND RETURNS ITS INDEX WITHIN THE memberList ARRAYLIST
	private int getMemberID()
	{
    	System.out.println("Please enter the member ID OR any other response to return to the Main Menu");
    	targetID = keyboardMain.nextLine();

    	// Get MemberID
    	for (int i = 0; i < memberList.length; i++)
    	{
    		// IF YOU ARE AT THE END OF THE ARRAY
    		if (i == memberList.length - 1)
    		{
    			System.out.println("Member " + targetID + " was not found");
    			return 99;
    		} 		
    		// SKIP OVER NULL ENTRIES IN THE ARRAY
    		else if (memberList[i] != null)
    		{
        		currentID = memberList[i].getID();
        		
        		// CHECK THE memberID OF THE TARGET MEMBER AGAINST THE targetID 
        		if (currentID.equals(targetID))
        		{
        			return i;	// return the index of the member and exit the loop
        		}
    		}
    		
    	} // END OF for (int i = 0; i < memberList.length; i++)
    	return 99;
	}
	
	
	// ALLOWS THE USER TO READ THE CURRENT INFORMATION, BEFORE RETURNING TO THE MAIN MENU
	private void pressAnyKey()
	{
    	System.out.println( " PRESS ENTER TO CONTINUE " ); 
    	keyboardMain.nextLine();
	}
	
	
	// ADDS THE SAMPLE DATA FROM PART B
	private void addSampleData()
	{
		// CREATE THE SAMPLE OBJECTS + ADD TO THE holdingList ARRAY
        holdingList[numberOfHoldings] = new Book("b000001", "Intro to Java");
        numberOfHoldings++;		
		
        holdingList[numberOfHoldings] = new Book("b000002", "Learning UML");
        numberOfHoldings++;		
        
        holdingList[numberOfHoldings] = new Book("b000003", "Design Patterns");
        numberOfHoldings++;		
        
        holdingList[numberOfHoldings] = new Book("b000004", "Advanced Java");
        numberOfHoldings++;		
        
        
        holdingList[numberOfHoldings] = new Video("v000001", "Java 1", 4);
        numberOfHoldings++;
        
        holdingList[numberOfHoldings] = new Video("v000002", "Java 2", 6);
        numberOfHoldings++;
        
        holdingList[numberOfHoldings] = new Video("v000003", "UML 1", 6);
        numberOfHoldings++;
        
        holdingList[numberOfHoldings] = new Video("v000004", "UML 2", 4);
        numberOfHoldings++;

        
        memberList[memberNo] = new StandardMember("s000001", "Joe Bloggs");
        memberNo++;

        memberList[memberNo] = new StandardMember("s000002", "Jane Smith");
        memberNo++;
        
        memberList[memberNo] = new PremiumMember("p000001", "Fred Bloggs");
        memberNo++;
        
        memberList[memberNo] = new PremiumMember("p000002", "Fred Smith");
        memberNo++;
        
	}
	
	// ### ERROR MESSAGES ###
	private void maxObjectsReached()	// TODO USE THIS AS ERROR CATCH METHOD ##############################
	{
    	System.out.println("# WARNING #");	                	
    	System.out.println("Unable to create a new Object, maximum number of Objects Reached");
    	System.out.println("");
	}
		
		
	


	
	
	// ################## NOT CURRENTLY USED #############################
	
	
	private String getValidatedHoldingID()	
	// CAREFULL WITH THE INITIAL CHARACTER, AS IT DOESNT DIFFERENTIATE BETWEEN BOOKS AND VIDEOS ATM!!!!!
	// USE THIS FOR WHEN THE USER NEEDS TO ENTER AN ID NUMBER. WHEN CREATING A NEW HOLDING, AUTO-GENERATE AN ID
	// 		USING THESE RULES, AND A CONSECUTIVE NUMBER  IE  LAST INT + 1 ETC    
	{
	    boolean validResponse = false;
	
	    // LOOP UNTIL THE USER ENTERS A VALID RESPONSE
	    while (validResponse == false)
	    {
	        // ASK THE USER TO ENTER THE HoldingID
	        System.out.println("HoldingID must be in the format of b000001 for Books or v000001 for Videos");      
	        holdingID = keyboardMain.nextLine();
	
	        if (holdingID.length() == 7)  // holdingID MUST BE 7 CHARACTERS LONG 
	        {	
	        	// FIRST CHARACTER MUST BE A 'b' or 'v'
	            if ((holdingID.charAt(0) == 'b') || (holdingID.charAt(0) == 'v'))
	            {
	                // CHECK FOR NON INTEGERS AFTER FIRST CHARACTER
	                for(int i = 1; i < holdingID.length(); i++)  
	                {
	                	if (!Character.isDigit(holdingID.charAt(i)))
	                    {
	                        System.out.println("Non integers detected within HoldingID after first character");
	                        i = holdingID.length();	// INVALID ID, NO NEED TO CONTINUE CHECKING THE REST OF THE ID
	                    }
	                	else if (i == holdingID.length() - 1)
	                	{
	                		// ONCE THE LOOP IS COMPLETE AND THE LAST CHARACTER HAS BEEN CHECKED THEN THE ID IS VALID
	                		validResponse = true;
	                	}
	                }  // END OF NON INTEGERS CHECK
	            }
	            else
	            {
	                System.out.println("The first character of the HoldingID MUST be either a 'b' (for Book Objects) or a 'v' (for Video Objects)");
	            }
	        }
	        else
	        {
	        	// NOT ENOUGH CHARACTERS DETECTED
	        	System.out.println("Not Enough Characters detected");
	        } 
	    }  // END OF while (validResponse == false)	    
        /* DONE check length    
        DONE check remaining characters 
        DONE check for first character
        DONE Check to see if already exists  - USING THE holdingNo
        DONE Check to see if it is the next available int  - USING THE holdingNo  ##### NOT QUITE RIGHT, BUT IT WILL DO
        */
	    return holdingID;
	}  // END OF public String getHoldingID()

}	// END OF THE MainMenu CLASS
